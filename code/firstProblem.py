import json
from datetime import datetime

def loadJson(path):
    f = open(path)
    data = json.load(f)
    f.close()
    return data

def gameDays(scoreboard):
    gameDates = []
    for date in scoreboard['results']:
        if len(scoreboard['results'][date]) != 0:
            gameDates.append(date)
    return gameDates

def retrieveDate(dayData):
    date_str = dayData.split()[0]
    date_obj = datetime.strptime(date_str, '%Y-%m-%d').strftime('%d-%m-%Y')
    return date_obj

def retrieveTime(dayData):
    time_str = dayData.split()[1]
    return time_str


def retrieveScoreboardData(scoreboard, date, event, eventObject):
    eventObject['event_id'] = scoreboard['results'][date]['data'][event]['event_id']
    eventObject['event_date'] = retrieveDate(scoreboard['results'][date]['data'][event]['event_date'])
    eventObject['event_time'] = retrieveTime(scoreboard['results'][date]['data'][event]['event_date'])
    eventObject['away_team_id'] = scoreboard['results'][date]['data'][event]['away_team_id']
    eventObject['away_nick_name'] = scoreboard['results'][date]['data'][event]['away_nick_name']
    eventObject['away_city'] = scoreboard['results'][date]['data'][event]['away_city']
    eventObject['home_team_id'] = scoreboard['results'][date]['data'][event]['home_team_id']
    eventObject['home_city'] = scoreboard['results'][date]['data'][event]['home_city']
    return eventObject

def retrieveTeamRankingsData(teamRankings, eventObject):
    awayID = eventObject['away_team_id']
    homeID = eventObject['home_team_id']
    for team in teamRankings['results']['data']:
        if team['team_id'] == awayID:
            eventObject['away_rank'] = team['rank']
            eventObject['away_rank_points'] = round(float(team['adjusted_points']),2)
        elif team['team_id'] == homeID:
            eventObject['home_rank'] = team['rank']
            eventObject['home_rank_points'] = round(float(team['adjusted_points']),2)
    return eventObject

def retrieveEvents(scoreboard, teamRankings):
    gameDates = gameDays(scoreboard)
    data = []
    for date in gameDates:
        eventObject = {}
        for event in scoreboard['results'][date]['data']:
            eventObject = retrieveScoreboardData(scoreboard, date, event, eventObject)
            eventObject = retrieveTeamRankingsData(teamRankings, eventObject)
            data.append(eventObject)
    return data

def preprocess(scoreboardPath, teamRankingsPath):
    scoreboard = loadJson(scoreboardPath)
    teamRankings = loadJson(teamRankingsPath)
    
    data = retrieveEvents(scoreboard, teamRankings)

    return data

if __name__ == '__main__':
    scoreboardPath = "../mocks/scoreboard.json"
    teamRankingsPath = "../mocks/teamRankings.json"

    data = preprocess(scoreboardPath, teamRankingsPath)
    with open('../mocks/resultsMocks.json', 'w') as f:
        json.dump(data, f, indent=4)







